package buscaminas;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/**
 * 
 * @author kuan
 *
 */
public class Buscaminas {
	
	/**
	 * Serveix per que el jugador introdueixi per teclat el que el joc demana.
	 */
	static Scanner sc = new Scanner(System.in);
	
	/**
	 * �s la matriu representada gr�ficament i els seus atributs
	 */
	static Board t = new Board();
	
	/**
	 * Complementa el board
	 */
	static Window f = new Window(t);

	public static void main(String[] args) {
		
		boolean sortir = false;
		
		boolean opciones = false;
		
		Jugador j1 = new Jugador();
		
		ArrayList<String> winners = new ArrayList<String>();
		
		int[][] tauler = new int[1][1];
		
		int mines = 0;
		//las minas descubiertas son el n�mero 10 porque he hecho la matriz de ints xd
		while (!sortir) {
			
			String opcion = menu();
			
			boolean valida = comprovaropcio(opcion);
			if (valida) {
				switch(opcion) {
					case "1":
						if (opciones) {
							boolean curs = true;
							int[][] fals = iniciartaulerfals(tauler);
							tauler = iniciarmines(tauler, mines);
							llegirmatriu(fals);
							while (curs) {
								inicialitzarGUI(mines);
								Posicio p = coordenadas(fals);
								fals = descobrir(p.fila, p.columna, mines, tauler, fals);
								llegirmatriu(fals);
								curs = comprovar(fals, tauler, p, mines, j1, winners);
							}
						}else {
							System.out.println("Falta configurar el joc en l'apartat d'opcions");
							System.out.println();
						}
						
					break;
					
					case "2":
						opciones = true;
						boolean sortir2 = false;
						while (!sortir2) {
							String opcion2 = opcions();
							boolean valida2 = comprovaropcio2(opcion2);
							if (valida2) {
								switch(opcion2) {
									case "1":
										j1 = canviarjugador(j1);
									break;
									case "2":
										tauler = canviartauler(tauler);
									break;
									case "3":
										mines = canviarmines(mines);
									break;
									case "4":
										sortir2 = true;
									break;
								}
							}
						}
						
					break;
						
					case "3":
						ajuda();
					break;
						
					case "4":
						winners(winners);
					break;
						
					case "5":
						sortir = exit();
					break;
				
				}
				
			}
			
		}

	}
	
	private static void inicialitzarGUI(int mines) {
        t.setColorbackground(0xb1adad);  //color del fons (si els colors estan desactivats). Els colors estan donats en hexa, i comencen per 0x (forma d�indicar de qu� es tracta d�un nombre en hexa)
        String[] lletres = {"","   1","   2","   3","   4","   5","   6","   7","   8","   *","   !"};  //qu� s'ha d'escriure en cada casella en base al nombre
        t.setText(lletres);  //passar la llista feta al taulell
        int[] colorlletres = {0x0000FF,0x00FF00,0xFFFF00,0xFF0000,0xFF00FF,0x00FFFF,0x521b98,0xFFFFFF,0xFF8000,0x7F00FF,0xFFFFFF};   //una llista de colors de lletres. La primera posici� ser� el color que correspongui al nombre 0, i aix�. 
        t.setColortext(colorlletres);  //passar la llista feta al taulell
        t.setActborder(true);
        String[] etiquetes2={"Mines: "+mines};    //crear etiquetes per al taulell. Les etiquetes son text que sortir� a la dreta del taulell
        f.setLabels(etiquetes2);  //actualitzar les etiquetes
        f.setActLabels(true);   //activar l�existencia d�etiquetes
        f.setTitle("Buscamines by Kuan");  //t�tol del programa.
             
	}
	
	/**
	 * Mostra una llista dels jugadors que han guanyat.
	 * @param winners
	 */
	private static void winners(ArrayList<String> winners) {
	
		System.out.println("Els jugadors que han guanyat son: ");
		System.out.println();
		System.out.println(winners);
		System.out.println();
		
	}
	
	/**
	 * Comprova si la condici� de una nova posici� en una matriu es v�lida o no.
	 * @param i
	 * @param j
	 * @param matriz
	 * @return
	 */
	private static boolean fuera(int i, int j, int[][] matriz) {
		if(i>=matriz.length || i<0 || j<0 || j>=matriz[0].length) {
			return true;
		}else {
			return false;
		}

	}

	/**
	 * Comprova si el joc ja ha finalitzat i si el jugador ha perdut/guanyat en funci� de la situaci� en el tauler i la posici� escollida.
	 * @param fals
	 * @param tauler
	 * @param p
	 * @param mines
	 * @param j
	 * @param winners
	 * @return
	 */
	private static boolean comprovar(int[][] fals, int[][] tauler, Posicio p, int mines, Jugador j, ArrayList<String> winners) {
		
		boolean curs = true;
		
		if (fals[p.fila][p.columna] == 10) {
			
			System.out.println("GAME OVER");
			System.out.println();
			curs = false;
			System.out.println("La colocaci� de les mines: ");
			System.out.println();
			for (int i=0;i<tauler.length;i++) {
				
				for (int k=0;k<tauler[0].length;k++) {
					
					if (tauler[i][k] == 1) {
						
						tauler[i][k] = 10;
						
					}
					
				}
				
			}
			
			llegirmatriu(tauler);
			
		}else {
			
			boolean sigue = false;
			
			for (int i=0;i<tauler.length;i++) {
				
				for (int k=0;k<tauler[0].length;k++) {
					
					if (fals[i][k] == 9) {
						
						if (tauler[i][k] == 0) {
							
							sigue = true;
							
						}
						
					}
					
				}
				
			}
			
			if (!sigue) {
				
				winners.add(j.nom);
				curs = false;
				System.out.println("Has guanyat, felicitats "+j.nom);
				System.out.println();
				
			}
			
		}
			
		return curs;
	}

	/**
	 * Destapa la casella escollida i les del voltant infinitament fins que trobi una casella amb contacte amb una mina.
	 * @param fila
	 * @param columna
	 * @param mines
	 * @param tauler
	 * @param fals
	 * @return
	 */
	private static int[][] descobrir(int fila, int columna, int mines, int[][] tauler, int[][] fals) {
		
		int contador = 0;
		
		if (tauler[fila][columna] == 1) {
			
			fals[fila][columna] = 10;
	
		}else {
			
			if (!fuera(fila+1,columna,fals)) {
				if (tauler[fila+1][columna] == 1) {
					contador++;
				}
			}
			
			if (!fuera(fila+1,columna+1,fals)) {		
				if (tauler[fila+1][columna+1] == 1) {
					contador++;
				}
			}
			
			if (!fuera(fila+1,columna-1,fals)) {
				if (tauler[fila+1][columna-1] == 1) {
					contador++;
				}
			}
			
			if (!fuera(fila,columna-1,fals)) {
				if (tauler[fila][columna-1] == 1) {
					contador++;
				}
			}
			
			if (!fuera(fila,columna+1,fals)) {
				if (tauler[fila][columna+1] == 1) {
					contador++;
				}
			}
						
			if (!fuera(fila-1,columna-1,fals)) {
				if (tauler[fila-1][columna-1] == 1) {
					contador++;
				}
			}
			
			if (!fuera(fila-1,columna,fals)) {
				if (tauler[fila-1][columna] == 1) {
					contador++;
				}
			}
			
			if (!fuera(fila-1,columna+1,fals)) {
				
				if (tauler[fila-1][columna+1] == 1) {
					contador++;
				}
			}
				
			fals[fila][columna] = contador;

		}
		
		if (contador == 0) {
			
			if (!fuera(fila+1,columna,fals) && fals[fila+1][columna] == 9) {
				
				descobrir(fila+1,columna,mines,tauler,fals);
				
			}
			
			if (!fuera(fila-1,columna,fals) && fals[fila-1][columna] == 9) {
				
				descobrir(fila-1,columna,mines,tauler,fals);
				
			}
			
			if (!fuera(fila,columna+1,fals) && fals[fila][columna+1] == 9) {
				
				descobrir(fila,columna+1,mines,tauler,fals);
				
			}
			
			if (!fuera(fila,columna-1,fals) && fals[fila][columna-1] == 9) {
				
				descobrir(fila,columna-1,mines,tauler,fals);
				
			}
			
			if (!fuera(fila-1,columna-1,fals) && fals[fila-1][columna-1] == 9) {
				
				descobrir(fila-1,columna-1,mines,tauler,fals);
				
			}
			
			if (!fuera(fila+1,columna-1,fals) && fals[fila+1][columna-1] == 9) {
				
				descobrir(fila+1,columna-1,mines,tauler,fals);
				
			}
			
			if (!fuera(fila+1,columna+1,fals) && fals[fila+1][columna+1] == 9) {
				
				descobrir(fila+1,columna+1,mines,tauler,fals);
				
			}
			
			if (!fuera(fila-1,columna+1,fals) && fals[fila-1][columna+1] == 9) {
				
				descobrir(fila-1,columna+1,mines,tauler,fals);
				
			}
			
		}
		
		return fals;
		
	}

	/**
	 * Demana al jugador una posici� X i Y en la matriu.
	 * @param tauler
	 * @return
	 */
	private static Posicio coordenadas(int[][] tauler) {
		
		boolean valida = false;
		
		Posicio p = new Posicio();
		
		while (!valida) {
			
			System.out.println("Introdueix la fila desitjada: ");
			System.out.println();
			p.fila = t.getMouseRow();
			System.out.println();
			System.out.println("Introdueix la columna desitjada: ");
			System.out.println();
			p.columna = t.getMouseCol();
			System.out.println();
			
			if (p.fila == -1 || p.columna == -1) {
				
			}else if (p.fila > tauler.length-1 || p.columna > tauler[0].length-1 || p.fila<0 || p.columna<0) {
				
				System.out.println("Posici� incorrecta. Torna a repetir");
				System.out.println();
				
			}else if (tauler[p.fila][p.columna] != 9) {
				
				System.out.println("Posici� incorrecta. Torna a repetir");
				System.out.println();
				
			}else {
				
				valida = true;
				
			}
			
		}
		
		return p;
		
	}

	/**
	 * Imprimeix per pantalla la matriu seleccionada. 
	 * @param tauler
	 */
	private static void llegirmatriu(int[][] tauler) {
		
		for (int i=0;i<tauler.length;i++) {
			
			for (int j=0;j<tauler[0].length;j++) {
				
				System.out.print(tauler[i][j]+" ");
				
			}System.out.println();
			
		}System.out.println();
		
		t.draw(tauler,'t');
		
	}

	/**
	 * Inicia el tauler fals segons les indicacions del jugador en l'apartat de configuraci�. S'omple amb 9.
	 * @param tauler
	 * @return
	 */
	private static int[][] iniciartaulerfals(int[][] tauler) {
		
		int[][] fals = new int[tauler.length][tauler[0].length];
		
		for (int i=0;i<fals.length;i++) {
			
			for (int j=0;j<fals[0].length;j++) {
				
				fals[i][j] = 9;
				
			}
			
		}
		
		return fals;
		
	}

	/**
	 * Inicia el tauler secret segons les indicacions del jugador en l'apartat de configuraci�. S'omple aleatoriament amb 0 i 1 amb una llista randomitzada.
	 * @param tauler
	 * @param mines
	 * @return
	 */
	private static int[][] iniciarmines(int[][] tauler, int mines) {
		
		int total = tauler.length * tauler[0].length;
		
		ArrayList<Integer> lista = new ArrayList<Integer>();
		
		for (int i=0;i<total;i++) {
			
			if (mines > 0) {
				
				lista.add(1);
				mines--;
				
			}else {
				
				lista.add(0);
				
			}
			
		}
		
		Collections.shuffle(lista);
		int contador = 0;
		
		for (int i=0;i<tauler.length;i++) {
			
			for (int j=0;j<tauler[0].length;j++) {
				
				tauler[i][j] = lista.get(contador);
				contador++;
				
			}
			
		}
		
		return tauler;
		
	}
	
	/**
	 * Ofereix al jugador l'oportunitat de canviar el seu nom.
	 * @param j1
	 * @return
	 */
	private static Jugador canviarjugador(Jugador j1) {
		
		System.out.println("Nom del jugador actual: "+j1.nom);
		System.out.println();
		System.out.println("Vols canviar-lo? (S/N)");
		System.out.println();
		boolean valida = false;
		while (!valida) {
			
			String opcion = sc.next().toLowerCase();
			System.out.println();
			if (opcion.equals("s")) {
				
				System.out.println("Introdueix el nou nom:");
				System.out.println();
				j1.nom = sc.next();
				System.out.println();
				System.out.println("El nou nom �s: "+j1.nom);
				System.out.println();
				valida = true;
				
			}else if (opcion.equals("n")) {
				
				valida = true;
				
			}else {
				
				System.out.println("Opci� incorrecta");
				System.out.println();
				
			}
			
		}
		
		return j1;
		
	}

	/**
	 * Ofereix al jugador l'oportunitat de canviar el tamany del tauler.
	 * @param tauler
	 * @return
	 */
	private static int[][] canviartauler(int[][] tauler) {
		
		System.out.println("Tamany del tauler actual: "+tauler.length+"x"+tauler[0].length);
		System.out.println();
		System.out.println("Vols canviar-lo? (S/N)");
		System.out.println();
		boolean valida = false;
		while (!valida) {
			
			String opcion = sc.next().toLowerCase();
			System.out.println();
			
			if (opcion.equals("s")) {
				
				boolean fvalida = false;
				boolean cvalida = false;
				
				int ofilas = tauler.length;
				int ocolumnas = tauler[0].length;
				
				System.out.println("Introdueix el nou nombre de files:");
				System.out.println();
				while (!fvalida) {
					
					ofilas = sc.nextInt();
					
					if (ofilas <= 0) {
						
						System.out.println("Les files tenen que ser un nombre positiu");
						System.out.println();
						System.out.println("Introdueix el nou nombre de files:");
						System.out.println();
						
					}else {
						
						fvalida = true;
						
					}
					
				}
				System.out.println();
				System.out.println("Introdueix el nou nombre de columnes:");
				System.out.println();
				while (!cvalida) {
					
					ocolumnas = sc.nextInt();
					
					if (ocolumnas <= 0) {
						
						System.out.println("Les columnes tenen que ser un nombre positiu");
						System.out.println();
						System.out.println("Introdueix el nou nombre de columnes:");
						System.out.println();
						
					}else {
						
						cvalida = true;
						
					}
					
				}
				
				System.out.println();
				System.out.println("El nou tauler �s de "+ofilas+"x"+ocolumnas);
				System.out.println();
				valida = true;
				
				tauler = new int[ofilas][ocolumnas];
				
			}else if (opcion.equals("n")) {
				
				valida = true;
				
			}else {
				
				System.out.println("Opci� incorrecta");
				System.out.println();
				
			}
			
		}
		
		return tauler;
		
	}

	/**
	 * Ofereix al jugador l'oportunitat de canviar el nombre de mines.
	 * @param mines
	 * @return
	 */
	private static int canviarmines(int mines) {
		
		System.out.println("Nombre de mines actuals: "+mines);
		System.out.println();
		System.out.println("Vols canviar-lo? (S/N)");
		System.out.println();
		boolean valida = false;
		while (!valida) {
			
			String opcion = sc.next().toLowerCase();
			System.out.println();
			
			if (opcion.equals("s")) {
				
				boolean mvalida = false;
				
				System.out.println("Introdueix el nou nombre de mines:");
				System.out.println();
				while (!mvalida) {
					
					mines = sc.nextInt();
					System.out.println();
					if (mines <= 0) {
						
						System.out.println("Les mines tenen que ser un nombre positiu");
						System.out.println();
						System.out.println("Introdueix el nou nombre de mines:");
						System.out.println();
						
					}else {
						
						mvalida = true;
						
					}
					
				}
				
				System.out.println("El nou nombre de mines �s: "+mines);
				System.out.println();
				valida = true;
				
			}else if (opcion.equals("n")) {
				
				valida = true;
				
			}else {
				
				System.out.println("Opci� incorrecta");
				System.out.println();
				
			}
			
		}
		
		return mines;
		
	}

	/**
	 * Comprova que no es pugui posar un nombre no v�lid en el men� d'opcions.
	 * @param opcion2
	 * @return
	 */
	private static boolean comprovaropcio2(String opcion2) {
		
		boolean valida = false;

		if (opcion2.equals("1") || opcion2.equals("2") || opcion2.equals("3") || opcion2.equals("4")) {
			
			valida = true;
			
		}
		
		if (valida) {
			
		}else {
			
			System.out.println("Opci� incorrecta");
			System.out.println();
			
		}
		
		return valida;
		
	}

	/**
	 * Inicia el men� d'opcions.
	 * @return
	 */
	private static String opcions() {
		
		System.out.println("1. Canviar nom");
		System.out.println("2. Canviar tamany");
		System.out.println("3. Canviar nombre de mines");
		System.out.println("4. Sortir");
		System.out.println();
		String opcion2 = sc.next();
		System.out.println();
		
		return opcion2;
		
	}

	/**
	 * Inicia el men� d'ajuda.
	 */
	private static void ajuda() {
		
		System.out.println("- El jugador t� que descubrir una casella en cada torn");
		System.out.println();
		System.out.println("- Si la casella no cont� una mina, indicar� quantes mines n'hi ha entre");
		System.out.println("  les vuit caselles adjacents");
		System.out.println();
		System.out.println("- El joc acaba quan les �niques caselles sense descubrir siguin les mines");
		System.out.println("  o si el jugador descobreix una mina");
		System.out.println();
		
	}

	/**
	 * Surt del joc.
	 * @return
	 */
	private static boolean exit() {
		
		System.out.println("Adi�s papaya");
		
		return true;
	}

	/**
	 * Inicia el men� principal.
	 * @return
	 */
	private static String menu() {
		
		System.out.println("BUSCAMINAS");
		System.out.println();
		System.out.println("1. Jugar");
		System.out.println("2. Opcions");
		System.out.println("3. Ajuda");
		System.out.println("4. Ranking");
		System.out.println("5. Sortir");
		System.out.println();
		String opcion = sc.next();
		System.out.println();
		
		return opcion;
		
	}

	/**
	 * Comprova que no es pugui posar un nombre no v�lid en el men� principal.
	 * @param opcion
	 * @return
	 */
	private static boolean comprovaropcio(String opcion) {
		
		boolean valida = false;

		if (opcion.equals("1") || opcion.equals("2") || opcion.equals("3") || opcion.equals("4") || opcion.equals("5")) {
			
			valida = true;
			
		}
		
		if (valida) {
			
		}else {
			
			System.out.println("Opci� incorrecta");
			System.out.println();
			
		}
		
		return valida;
		
	}

}
