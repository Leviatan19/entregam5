package gatoenraya;

import java.util.Scanner;

public class GatoEnRaya {

	static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		int casos = sc.nextInt();
		
		for (int u=0;u<casos;u++) {
			
			int filas = sc.nextInt();
			
			int columnas = sc.nextInt();
			
			int[][] mat = new int[filas][columnas];
			
			for (int k=0;k<mat.length;k++) {
				
				for (int j=0;j<mat[0].length;j++) {
					
					mat[k][j] = sc.nextInt();
					
				}
				
			}
			
			System.out.println(gato(mat));
			
		}

	}
	
	public static int gato (int[][] mat) {
		
		int contador = 0;
		
		for (int i=0;i<mat.length;i++) {
			
			for (int j=0;j<mat[0].length;j++) {
				
				boolean salir = false;
				
				if (mat[i][j] == 1) {
					if (!salir) {
						if (!fuera(i+1,j,mat)) {
							if (mat[i+1][j] == 1) {
								if (!fuera(i-1,j,mat)) {
									if (mat[i-1][j] == 1) {
										contador++;
										mat[i+1][j] = 0;
										mat[i-1][j] = 0;
										mat[i][j] = 0;
										salir = true;
									}
								}
							}
						}
					}
					if (!salir) {
						if (!fuera(i,j+1,mat)) {
							if (mat[i][j+1] == 1) {
								if (!fuera(i,j-1,mat)) {
									if (mat[i][j-1] == 1) {
										contador++;
										mat[i][j+1] = 0;
										mat[i][j-1] = 0;
										mat[i][j] = 0;
										salir = true;
									}
								}
							}
						}
					}
					if (!salir) {
						if (!fuera(i+1,j+1,mat)) {
							if (mat[i+1][j+1] == 1) {
								if (!fuera(i-1,j-1,mat)){
									if (mat[i-1][j-1] == 1) {
										contador++;
										mat[i+1][j+1] = 0;
										mat[i-1][j-1] = 0;
										mat[i][j] = 0;
										salir = true;
									}
								}
							}
						}
					}
					if (!salir) {
						if (!fuera(i+1,j-1,mat)) {
							if (mat[i+1][j-1] == 1) {
								if (!fuera(i-1,j+1,mat)){
									if (mat[i-1][j+1] == 1) {
										contador++;
										mat[i][j+1] = 0;
										mat[i][j-1] = 0;
										mat[i][j] = 0;
										salir = true;
									}
								}
							}
						}
					}
				}
			}
		}
		
		return contador;
		
	}
	
	public static boolean fuera(int i, int j, int[][] matriz) {
		if(i>=matriz.length || i<0 || j<0 || j>=matriz[0].length) {
			return true;
		}else {
			return false;
		}

	}

}
