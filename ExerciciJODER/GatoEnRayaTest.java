package gatoenraya;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class GatoEnRayaTest {
	
	int[][] mat1 = {{1, 0, 1},{1, 1, 1},{1, 1, 1}};
	int[][] mat2 = {{1, 1, 1},{0, 0, 0},{1, 1, 1}};
	int[][] mat3 = {{1,0,1},{0,1,1},{1,1,1}};
	int[][] mat4 = {{1,1,1,1},{1,0,0,1},{1,1,1,1},{0,0,1,1}};
	int[][] mat5 = {{1, 1, 1, 0, 1},{0, 1, 1, 1, 1},{1, 0, 1, 0, 1},{1, 0, 0, 1, 1},{1, 1, 1, 0, 1}};
	int[][] mat6 = {{1, 1, 1},{1, 0, 1},{1, 1, 1}};

	@Test
	void testFacil() {
		assertEquals(2,GatoEnRaya.gato(mat1));
		assertEquals(2,GatoEnRaya.gato(mat2));
		assertEquals(1,GatoEnRaya.gato(mat3));
	}

	@Test
	void testDificil() {
		assertEquals(3,GatoEnRaya.gato(mat4));
		assertEquals(5,GatoEnRaya.gato(mat5));
		assertEquals(2,GatoEnRaya.gato(mat6));
	}

}
